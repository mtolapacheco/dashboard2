import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-bootstrap-main',
  templateUrl: './ng-bootstrap-main.component.html',
  styleUrls: ['./ng-bootstrap-main.component.scss']
})
export class NgBootstrapMainComponent implements OnInit {

  public title: string;
  constructor(private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this._activatedRoute.data.subscribe((data: { title: string, second: string }) => {
      if (data) {
        this.title = data.title;
      }
    });
  }

}

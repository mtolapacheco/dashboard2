import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimeNgChildComponent } from './prime-ng-child.component';

describe('PrimeNgChildComponent', () => {
  let component: PrimeNgChildComponent;
  let fixture: ComponentFixture<PrimeNgChildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimeNgChildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimeNgChildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

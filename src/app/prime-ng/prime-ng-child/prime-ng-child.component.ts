import { ActivatedRoute, ParamMap } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { PrimeNgDetailService } from '../prime-ng-detail/prime-ng-detail.service';

@Component({
  selector: 'app-prime-ng-child',
  templateUrl: './prime-ng-child.component.html',
  styleUrls: ['./prime-ng-child.component.scss']
})
export class PrimeNgChildComponent implements OnInit {

  public power: {id: number, name: string, description: string };
  constructor(private _primeNgDetailService: PrimeNgDetailService,
              private _activatedRoute: ActivatedRoute) {
}

  ngOnInit() {
    this._activatedRoute.paramMap.subscribe((params: ParamMap) => {
      let id = params.get('id');
      this.power = this._primeNgDetailService.getById(+id);
    });
  }

}

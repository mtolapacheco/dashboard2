import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { PrimeNgDetailService } from './prime-ng-detail.service';

@Component({
  selector: 'app-prime-ng-detail',
  templateUrl: './prime-ng-detail.component.html',
  styleUrls: ['./prime-ng-detail.component.scss']
})
export class PrimeNgDetailComponent implements OnInit {
  public powers: {id: number, name: string, description: string }[];

  constructor(private _primeNgDetailService: PrimeNgDetailService,private _router: Router) {
    this.powers = [];
  }

  ngOnInit() {
    this.powers = this._primeNgDetailService.powers;
  }

  public navigate(id: number): void {
    this._router.navigate(['prime-ng/detail', id]);
  }
}

import { TestBed } from '@angular/core/testing';

import { PrimeNgDetailService } from './prime-ng-detail.service';

describe('PrimeNgDetailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PrimeNgDetailService = TestBed.get(PrimeNgDetailService);
    expect(service).toBeTruthy();
  });
});

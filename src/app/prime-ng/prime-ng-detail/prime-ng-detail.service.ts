import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PrimeNgDetailService {

  private _powers: { id: number, name: string, description: string }[];
  constructor() {
    this._powers = [
      {
        id: 1,
        name: 'goku',
        description: 'he is a low class warrior'
      },
      {
        id: 2,
        name: 'vegeta',
        description: 'he is the king of warriors'
      },
      {
        id: 3,
        name: 'trunks',
        description: 'he is the son of the king'
      },
      {
        id: 4,
        name: 'gohan',
        description: 'he is a great warrior'
      }
    ];
  }

  public getById(id: number): { id: number, name: string, description: string } {
    let response: {id: number, name: string, description: string } = null;
    for (let power of this._powers) {
      if (power.id === id) {
        response = power;
      }
    }
    return response;
  }
  get powers(): {id: number, name: string, description: string }[] {
    return this._powers;
  }
}

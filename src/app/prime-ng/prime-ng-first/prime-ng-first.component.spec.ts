import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimeNgFirstComponent } from './prime-ng-first.component';

describe('PrimeNgFirstComponent', () => {
  let component: PrimeNgFirstComponent;
  let fixture: ComponentFixture<PrimeNgFirstComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimeNgFirstComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimeNgFirstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

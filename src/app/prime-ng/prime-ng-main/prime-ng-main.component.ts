import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-prime-ng-main',
  templateUrl: './prime-ng-main.component.html',
  styleUrls: ['./prime-ng-main.component.scss']
})
export class PrimeNgMainComponent implements OnInit {

  public title: string;

  constructor(private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this._activatedRoute.data.subscribe((data: { title: string, second: string }) => {
      if (data) {
        this.title = data.title;
      }
    });
  }

}

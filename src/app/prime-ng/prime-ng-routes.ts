import { Routes } from '@angular/router';
import { PrimeNgMainComponent } from './prime-ng-main/prime-ng-main.component';
import { PrimeNgFirstComponent } from './prime-ng-first/prime-ng-first.component';
import { PrimeNgDetailComponent } from './prime-ng-detail/prime-ng-detail.component';
import { PrimeNgChildComponent } from './prime-ng-child/prime-ng-child.component';

export const PRIME_NG_ROUTES_CONFIG: Routes = [
  {
    path: '',
    component: PrimeNgMainComponent,
    data: {title: 'Laboratorio 3'},
    children: [
      {
        path: 'data',
        component: PrimeNgFirstComponent
      },
      {
        path: 'detail',
        component: PrimeNgDetailComponent,
        children: [
          {
            path: ':id',
            component: PrimeNgChildComponent
          }
        ]
      }
    ]
  }
];

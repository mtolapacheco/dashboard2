import { PRIME_NG_ROUTES_CONFIG } from './prime-ng-routes';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forChild(PRIME_NG_ROUTES_CONFIG)],
  exports: [RouterModule]
})
export class PrimeNgRoutingModule { }

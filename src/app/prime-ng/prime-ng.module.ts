import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrimeNgRoutingModule } from './prime-ng-routing.module';
import { PrimeNgMainComponent } from './prime-ng-main/prime-ng-main.component';
import { PrimeNgFirstComponent } from './prime-ng-first/prime-ng-first.component';
import { PrimeNgDetailComponent } from './prime-ng-detail/prime-ng-detail.component';
import { PrimeNgChildComponent } from './prime-ng-child/prime-ng-child.component';

@NgModule({
  declarations: [PrimeNgMainComponent, PrimeNgFirstComponent, PrimeNgDetailComponent, PrimeNgChildComponent],
  imports: [
    CommonModule,
    PrimeNgRoutingModule
  ]
})
export class PrimeNgModule { }
